package io.github.dxrkm.menu.listener;

import io.github.dxrkm.menu.Menu;
import io.github.dxrkm.menu.MenuSlot;
import org.bukkit.entity.Player;
import org.bukkit.event.Cancellable;
import org.bukkit.event.HandlerList;
import org.bukkit.event.player.PlayerEvent;
import org.jetbrains.annotations.NotNull;

public class MenuClickEvent extends PlayerEvent implements Cancellable {

    private Player player;
    private Menu menu;
    private MenuSlot menuSlot;
    private static final HandlerList handlers = new HandlerList();

    public MenuClickEvent(@NotNull Player who, boolean async) {
        super(who, async);
    }

    public MenuClickEvent(@NotNull Player who,@NotNull MenuSlot menuSlot) {
        super(who);
        this.menu = menuSlot.menu();
        this.menuSlot = menuSlot;
    }

    @Override
    public boolean isCancelled() {
        return false;
    }

    @Override
    public void setCancelled(boolean cancel) {

    }

    @Override
    public @NotNull HandlerList getHandlers() {
        return handlers;
    }

    public static HandlerList getHandlerList() {
        return handlers;
    }

    public Menu menu() {
        return menu;
    }

    public void menu(Menu menu) {
        this.menu = menu;
    }

    public MenuSlot menuSlot() {
        return menuSlot;
    }

    public void menuSlot(MenuSlot menuSlot) {
        this.menuSlot = menuSlot;
    }
}
