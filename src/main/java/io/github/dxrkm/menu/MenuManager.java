package io.github.dxrkm.menu;

import org.bukkit.entity.Player;

import java.util.HashMap;
import java.util.Map;

public class MenuManager {

    private static final Map<Player, Menu> menuOpened = new HashMap<>();

    protected MenuManager() {

    }

    public void setCurrentGui(Player player, Menu menu) {
        menuOpened.put(player, menu);
    }

    public Menu menu(Player player) {
        if (menuOpened.containsKey(player)) {
            return menuOpened.get(player);
        }
        return null;
    }
}
