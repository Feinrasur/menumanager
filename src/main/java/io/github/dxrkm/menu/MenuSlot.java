package io.github.dxrkm.menu;

import net.kyori.adventure.text.Component;
import org.bukkit.Material;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

public class MenuSlot {

    private final Menu menu;
    private SlotAction slotAction;
    private ItemStack slotItem;
    private final int slot;

    private String permission = "";

    public MenuSlot(Menu menu, int slot, Material material, Component name, SlotAction slotAction) {
        this.menu = menu;
        this.slot = slot;
        this.slotAction = slotAction;
        ItemStack item = new ItemStack(material);
        ItemMeta meta = item.getItemMeta();
        meta.displayName(name);
        item.setItemMeta(meta);
        this.slotItem = item;
    }

    public MenuSlot(Menu menu, int slot, Material material, SlotAction slotAction) {
        this.menu = menu;
        this.slot = slot;
        this.slotAction = slotAction;
        this.slotItem = new ItemStack(material);
    }

    public MenuSlot(Menu menu, int slot, ItemStack itemStack, SlotAction slotAction) {
        this.menu = menu;
        this.slot = slot;
        this.slotAction = slotAction;
        this.slotItem = itemStack;
    }
    /* Constructor end */

    /* Content start */
    public Menu menu() {
        return this.menu;
    }

    public void action(SlotAction menuAction) {
        this.slotAction = menuAction;
    }

    public SlotAction action() {
        return this.slotAction;
    }


    public void item(ItemStack itemStack) {
        this.slotItem = itemStack;
    }

    public void item(Material material, Component name) {
        ItemStack item = new ItemStack(material);
        ItemMeta meta = item.getItemMeta();
        meta.displayName(name);
        item.setItemMeta(meta);
        this.slotItem = item;
    }

    public ItemStack item() {
        return this.slotItem;
    }

    public Integer slot() {
        return slot;
    }

    public boolean removeItem() {
        if (menu.inventory().contains(this.slotItem)) {
            menu.inventory().remove(this.slotItem);
            return true;
        }
        return false;
    }

    public boolean remove() {
        menu.removeSlot(this.slot);
        if (menu.inventory().contains(this.slotItem)) {
            menu.inventory().remove(this.slotItem);
            return true;
        }
        return false;
    }







    /* Content end */


    /* Configuration start */
    public MenuSlot permission(String permission) {
        this.permission = permission;
        return this;
    }

    public String permission() {
        return this.permission;
    }


}
