package io.github.dxrkm.menu;

import net.kyori.adventure.text.Component;
import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.plugin.java.JavaPlugin;
import org.jetbrains.annotations.Nullable;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.stream.Collectors;

public abstract class Menu {

    protected static MenuManager instance;
    protected static JavaPlugin plugin;

    private final HashMap<Integer, MenuSlot> slots = new HashMap<>();
    private Inventory inventory;
    private final int size;

    private Menu previous = null;
    private Menu next = null;

    private boolean clickSound = true;

    public static void register(JavaPlugin plugin) {
        Menu.plugin = plugin;
        instance = new MenuManager();
        plugin.getServer().getPluginManager().registerEvents(new MenuListener(), plugin);
    }

    public Menu(MenuRows rows, Component title) {
        inventory = Bukkit.createInventory(null, rows.value(), title);
        this.size = rows.value();
    }

    public int size() {
        return this.size;
    }

    public MenuSlot set(int slot, Material material, SlotAction slotAction) {
        MenuSlot menuSlot = new MenuSlot(this, slot, material, slotAction);
        slots.put(slot, menuSlot);
        this.inventory.setItem(slot, new ItemStack(material));
        return menuSlot;
    }

    public MenuSlot set(int slot, ItemStack itemStack, SlotAction slotAction) {
        MenuSlot menuSlot = new MenuSlot(this, slot, itemStack, slotAction);
        slots.put(slot, menuSlot);
        this.inventory.setItem(slot, itemStack);
        return menuSlot;
    }

    public MenuSlot set(int slot, ItemStack itemStack, Component text, SlotAction slotAction) {
        ItemMeta meta = itemStack.getItemMeta();
        meta.displayName(text);
        itemStack.setItemMeta(meta);
        MenuSlot menuSlot = new MenuSlot(this, slot, itemStack, slotAction);
        slots.put(slot, menuSlot);
        this.inventory.setItem(slot, itemStack);
        return menuSlot;
    }

    public MenuSlot add(ItemStack itemStack, SlotAction slotAction) {
        int freeSlot = inventory.firstEmpty();
        if (freeSlot >= 0) {
            MenuSlot menuSlot = new MenuSlot(this, freeSlot, itemStack, slotAction);
            slots.put(freeSlot, menuSlot);
            inventory.setItem(freeSlot, itemStack);
            return menuSlot;
        }
        return null;
    }

    public MenuSlot add(Material material, SlotAction slotAction) {
        int freeSlot = inventory.firstEmpty();
        if (freeSlot >= 0) {
            MenuSlot menuSlot = new MenuSlot(this, freeSlot, new ItemStack(material), slotAction);
            slots.put(freeSlot, menuSlot);
            inventory.setItem(freeSlot, new ItemStack(material));
            return menuSlot;
        }
        return null;
    }

    public MenuSlot add(ItemStack itemStack, Component text, SlotAction slotAction) {
        ItemMeta meta = itemStack.getItemMeta();
        meta.displayName(text);
        itemStack.setItemMeta(meta);
        int freeSlot = inventory.firstEmpty();
        if (freeSlot >= 0) {
            MenuSlot menuSlot = new MenuSlot(this, freeSlot, itemStack, slotAction);
            slots.put(freeSlot, menuSlot);
            inventory.setItem(freeSlot, itemStack);
            return menuSlot;
        }
        return null;
    }

    public MenuSlot add(Material material, Component text, SlotAction slotAction) {
        ItemStack itemStack = new ItemStack(material);
        ItemMeta meta = itemStack.getItemMeta();
        meta.displayName(text);
        itemStack.setItemMeta(meta);
        int freeSlot = inventory.firstEmpty();
        if (freeSlot >= 0) {
            MenuSlot menuSlot = new MenuSlot(this, freeSlot, itemStack, slotAction);
            slots.put(freeSlot, menuSlot);
            inventory.setItem(freeSlot, itemStack);
            return menuSlot;
        }
        return null;
    }

    public MenuSlot menuSlot(int slot) {
        return slots.get(slot);
    }

    private List<@Nullable MenuSlot> getSlotsByRow(MenuRows rows) {
        List<@Nullable MenuSlot> slots = new ArrayList<>();
        int start = (rows.value() - 1) * 9;
        int end = rows.value() * 9;
        for (int i = start; i < end; i++) {
            slots.add(this.slots.get(i));
        }
        return slots;
    }

    @Deprecated
    private List<MenuSlot> getSlotsByRowApi(MenuRows rows) {
        int start = (rows.value() - 1) * 9;
        int end = rows.value() * 9;
        return this.slots.values().stream()
                .skip(start)
                .limit(end - start)
                .collect(Collectors.toList());
    }

    public void overwriteInventory(Inventory inventory) {
        this.inventory = inventory;
    }

    public Inventory inventory() {
        return this.inventory;
    }

    public void removeSlot(int slot) {
        this.slots.remove(slot);
    }

    public void open(Player player) {
        player.openInventory(inventory());
        instance.setCurrentGui(player, this);
    }

    public Menu previous() {
        return previous;
    }

    public Menu next() {
        return next;
    }

    public void previous(Menu menu) {
        this.previous = menu;
    }

    public void next(Menu menu) {
        this.next = menu;
    }

    public boolean clickSound() {
        return clickSound;
    }

    public void clickSound(boolean rule) {
        this.clickSound = rule;
    }

}
