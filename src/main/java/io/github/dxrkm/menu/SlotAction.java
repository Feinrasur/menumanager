package io.github.dxrkm.menu;

import io.github.dxrkm.menu.listener.MenuClickEvent;

public interface SlotAction {

    void run(MenuClickEvent menuClickEvent);
}
