package io.github.dxrkm.menu;

import io.github.dxrkm.menu.listener.MenuClickEvent;
import org.bukkit.Bukkit;
import org.bukkit.Sound;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.ClickType;
import org.bukkit.event.inventory.InventoryAction;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.inventory.InventoryDragEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.InventoryView;

public class MenuListener implements Listener {

    @EventHandler
    public void onInvDrag(InventoryDragEvent event) {
        if (!(event.getWhoClicked() instanceof Player player))
            return;
        Menu menu = Menu.instance.menu(player);
        if (menu == null) return;
        if (event.getInventory().equals(menu.inventory())) {
            event.setCancelled(true);
        }
    }

    @EventHandler(priority = EventPriority.LOWEST, ignoreCancelled = true)
    public void inventoryClickEvent(InventoryClickEvent event) {
        if (!(event.getWhoClicked() instanceof Player player)) return;
        Menu menu = Menu.instance.menu(player);
        if (menu == null) return;
        Inventory guiInv = menu.inventory();
        if (guiInv == null) return;
        InventoryView view = player.getOpenInventory();
        Inventory topInv = view.getTopInventory();
        Inventory eventInv = event.getClickedInventory();
        if (eventInv == null) return;
        Inventory playerInv = player.getInventory();

        if (eventInv.equals(playerInv) && topInv.equals(guiInv)) {
            if (event.getAction().equals(InventoryAction.MOVE_TO_OTHER_INVENTORY)) {
                event.setCancelled(true);
                return;
            }
            Bukkit.getScheduler().runTaskLater(Menu.plugin, r -> {
                player.getInventory().setItemInOffHand(player.getInventory().getItemInOffHand());
            }, 20);
            return;
        }
        if (!eventInv.equals(guiInv)) return;
        Integer slot = event.getSlot();
        if (slot == null) return;
        if (slot == -1) return;
        if (event.getClickedInventory().equals(guiInv))
            event.setCancelled(true);

        if (menu.menuSlot(slot) != null)
            Bukkit.getPluginManager().callEvent(new MenuClickEvent(player, menu.menuSlot(slot)));

    }

    @EventHandler
    public void onMenuClick(MenuClickEvent event) {
        MenuSlot slot = event.menuSlot();
        if (slot == null) return;
        Player player = event.getPlayer();
        if (player.hasPermission(slot.permission()) || slot.permission().equals("")) {
            if (slot.action() != null) {
                slot.action().run(event);
                if (event.menu().clickSound())
                    player.playSound(player.getLocation(), Sound.UI_BUTTON_CLICK, 2, 1);
            }
        }

    }
}
