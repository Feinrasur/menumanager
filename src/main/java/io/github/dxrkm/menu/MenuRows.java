package io.github.dxrkm.menu;

public enum MenuRows {

    ONE(9),
    TWO(18),
    THREE(27),
    FOUR(36),
    FIVE(45),
    SIX(54);

    private Integer value;

    MenuRows(Integer value) {
        this.value = value;
    }

    public Integer value() {
        return this.value;
    }


}
